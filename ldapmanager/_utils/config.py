import os
import yaml


def load_config(ctx, param, config_path):
    # pylint: disable=unused-argument
    configpath = os.path.abspath(os.path.expanduser(config_path))

    with open(configpath, 'r') as infile:
        config = yaml.safe_load(infile.read())

    try:
        options = dict(config['defaults'])
    except KeyError:
        options = {}

    ctx.default_map = options
    ctx.obj = {
        'config': config,
    }
