import os.path

import click
import ldap3

from ._utils.core import DynamicMultiCommandFactory
from ._utils.config import load_config

CONTEXT_SETTINGS = {'help_option_names': ['-h', '--help'], 'max_content_width': 120}
DynamicMultiCommand = DynamicMultiCommandFactory().create(__file__, __package__)


@click.command(context_settings=CONTEXT_SETTINGS)
@click.option(
    '-c', '--config', 'config_path',
    type=click.Path(exists=True, dir_okay=False),
    default=os.path.expanduser('~/.config/ldapmanager.yml'),
    help='Configuration file in YAML format',
    callback=load_config,
    is_eager=True,
    expose_value=False,
    show_default=True,
)
@click.option(
    '-s', '--server', 'server',
    type=click.STRING,
    help='Server to be used from config file',
    show_default=True
)
@click.pass_obj
def cli(obj, server):
    """LDAP management utility."""
    config = obj['config']
    try:
        ldap_uri = config['servers'][server]['uri']
        ldap_username = config['servers'][server]['username']
        ldap_password = config['servers'][server]['password']
    except KeyError as error:
        raise click.ClickException(f"Missing Key in Config Section 'ldap': {error}.") from error

    ldap_server = ldap3.Server(ldap_uri, get_info='ALL')
    print(ldap_server)
    ldap_conn = ldap3.Connection(ldap_server, user=ldap_username, password=ldap_password, client_strategy=ldap3.SAFE_SYNC)
    ldap_conn.start_tls()
    ldap_conn.bind()
    print(ldap_conn)
